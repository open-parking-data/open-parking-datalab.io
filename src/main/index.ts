
import * as fs from "fs";
import { HsmlFragment } from "peryl/dist/hsml";
import { hsmls2htmls, } from "peryl/dist/hsml-html";
import { page } from "./components/page";
import { appshell } from "./components/appshell";
import { sidebar, Menu } from "./components/sidebar";
import { intro } from "./components/intro";
import { team } from "./components/team";
import { partners } from "./components/partners";
import { news } from "./components/news";

function HTML(file: string, jsonmls: HsmlFragment, pretty = false): void {
    const path = __dirname + "/../../dist/" + file;
    console.log("generate:", path);
    const html = hsmls2htmls(jsonmls, pretty).join("");
    fs.writeFileSync(path, html);
}

const pretty = true;

const baseUrl = "https://open-parking-data.gitlab.io";
const siteTitle = "Open Parking Data";
const lang = "en";
const description = "Open Parking Data community portal";
const keywords = "open, parking, data, community, portal";
const image = `${baseUrl}/assets/icons/android-chrome-512x512.png`;
const menu: Menu[] = [
    { url: "index.html", label: "Intro", icon: "i.fas.fa-fw.fa-info" },
    { url: "team.html", label: "Team", icon: "i.fas.fa-fw.fa-users" },
    { url: "partners.html", label: "Partners", icon: "i.fas.fa-fw.fa-handshake" }
    // { url: "news.html", label: "News", icon: "i.fas.fa-fw.fa-bell" }
];

let file = "index.html";
let title = "Intro";
HTML(file,
    page({
        lang,
        absUrl: `${baseUrl}/${file}`,
        title: `${title} | ${siteTitle}`,
        description,
        keywords,
        image,
        content: appshell(siteTitle, title,
            sidebar(menu, file),
            intro())
    }),
    pretty
);

file = "team.html";
title = "Team";
HTML(file,
    page({
        lang,
        absUrl: `${baseUrl}/${file}`,
        title: `${title} | ${siteTitle}`,
        description,
        keywords,
        image,
        content: appshell(siteTitle, title,
            sidebar(menu, file),
            team())
    }),
    pretty
);

file = "partners.html";
title = "Partners";
HTML(file,
    page({
        lang,
        absUrl: `${baseUrl}/${file}`,
        title: `${title} | ${siteTitle}`,
        description,
        keywords,
        image,
        content: appshell(siteTitle, title,
            sidebar(menu, file),
            partners())
    }),
    pretty
);

file = "news.html";
title = "News";
HTML(file,
    page({
        lang,
        absUrl: `${baseUrl}/${file}`,
        title: `${title} | ${siteTitle}`,
        description,
        keywords,
        image,
        content: appshell(siteTitle, title,
            sidebar(menu, file),
            news())
    }),
    pretty
);
