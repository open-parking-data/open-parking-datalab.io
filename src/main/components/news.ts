import { HsmlFragment } from "peryl/dist/hsml";

export function news(): HsmlFragment {
        // http://inrix.com/blog/2018/10/inrix-parking-data-coverage-expansion/
    return [
        ["div.w3-content", {}, [
            ["h1", ["News"]],
            ["dl", [
                ["dt.w3-text-grey", ["Mar 18, 2019"]],
                ["dd.w3-margin-bottom", [
                    `We are preparing an application for a non-refundable financial
                    contribution within the call Computer vision for open parking data`
                ]],
                ["dt.w3-text-grey", ["Mar 4, 2019"]],
                ["dd.w3-margin-bottom", [`
                    Start cooperation with`,
                    ["a",
                        {
                            href: "https://dai.fmph.uniba.sk/",
                            target: "_blank",
                            rel: "noopener"
                        },
                        [
                            `Department of Applied Informatics
                            FMFI UK Bratislava Slovakia`
                        ]
                    ]
                ]],
                ["dt.w3-text-grey", ["Feb 14, 2019"]],
                ["dd.w3-margin-bottom", [
                    `Project simple web page created`
                ]],
                ["dt.w3-text-grey", ["Oct 22, 2019"]],
                ["dd.w3-margin-bottom", [
                    `GitLab repository created`
                ]]
            ]]
        ]]
    ];
}
