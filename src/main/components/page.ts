import { HsmlFragment } from "peryl/dist/hsml";

export interface PageState {
    lang: string;
    absUrl: string;
    title: string;
    description: string;
    keywords: string;
    image: string;
    content: HsmlFragment;
}

export function page(state: PageState): HsmlFragment {
    return [
        "<!DOCTYPE html>",
        ["html", { lang: "en" }, [
            ["head", [
                ["meta", { charset: "utf-8" }],
                ["meta", { "http-equiv": "X-UA-Compatible", content: "IE=edge,chrome=1" }],
                ["meta", { name: "viewport", content: "width=device-width, initial-scale=1.0, maximum-scale=5.0" }],

                ["title", [state.title]],

                ["meta", { name: "description", content: state.description }],
                ["meta", { name: "keywords", content: state.keywords }],
                ["meta", { name: "author", content: "Peter Rybar, pr.rybar@gmail.com, https://gitlab.com/peter-rybar" }],
                ["meta", { name: "robots", content: "follow, all" }],

                ["link", { rel: "stylesheet", href: "https://www.w3schools.com/w3css/4/w3.css" }],
                ["link", { rel: "stylesheet", href: "https://fonts.googleapis.com/css?family=Raleway" }],
                // ["link", { rel: "stylesheet", href: "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" }],
                ["link", { rel: "stylesheet", href: "https://use.fontawesome.com/releases/v5.7.1/css/all.css" }],
                // ["link", { rel: "stylesheet", href: "assets/css/styles.css" }],

                ["style", [`html, body, h1, h2, h3, h4, h5 { font-family: "Raleway", sans-serif }`]],

                ["link", { rel: "icon", href: "/favicon.ico", type: "image/x-icon" }],
                ["link", { rel: "manifest", href: "manifest.json" }],
                ["meta", { id: "theme-color", name: "theme-color", content: "#2196f3" }],
                ["meta", { name: "apple-mobile-web-app-capable", content: "yes" }],

                ["meta", { property: "og:type", content: "website" }],
                ["meta", { property: "og:locale", content: state.lang }],
                ["meta", { property: "og:site_name", content: state.title }],
                ["meta", { property: "og:title", content: state.title }],
                ["meta", { property: "og:description", content: state.description ? state.description : "" }],
                ["meta", { property: "og:url", content: state.absUrl }],
                ["meta", { property: "og:image", content: state.image }],
                ["meta", { property: "og:image:secure_url", content: state.image }],
                ["meta", { property: "og:image:width", content: "401" }],
                ["meta", { property: "og:image:height", content: "401" }],
                ["meta", { property: "og:image:alt", content: "Open Parking Data" }],
                // ["meta", { property: "og:updated_time", content: "2018-07-30T17:07:34-07:00" }],
                // ["meta", { property: "fb:app_id", content: "315060158846201" }],
                ["meta", { name: "twitter:card", content: "summary" /*"summary_large_image"*/}],
                ["meta", { name: "twitter:title", content: state.title }],
                ["meta", { name: "twitter:description", content: state.description ? state.description : "" }],
                ["meta", { name: "twitter:image", content: state.image }],
                ["meta", { name: "twitter:site", content: "@open-parking-data" }],
                ["meta", { name: "twitter:creator", content: "@open-parking-data" }],

                // <!-- Global site tag (gtag.js) - Google Analytics -->
                ["script", { async: true, src: "https://www.googletagmanager.com/gtag/js?id=UA-135634596-1" }],
                ["script", [`
                    window.dataLayer = window.dataLayer || [];
                    function gtag(){dataLayer.push(arguments);}
                    gtag('js', new Date());
                    gtag('config', 'UA-135634596-1');
                `
                ]]
            ]],
            ["body.w3-light-grey-", [
                ...state.content,
                ["script", { src: "service-worker-init.js" }]
            ]]
        ]]
    ];
}
