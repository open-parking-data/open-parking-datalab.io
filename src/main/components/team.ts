import { HsmlFragment, HsmlElement } from "peryl/dist/hsml";

const founders = [
    {
        name: "Peter Rybár",
        position: "Founder, CEO",
        bio: `Peter absolvoval Fakultu matematiky a fyziky Univerzity Komenského v Bratislave, kde študoval fyziku. Počas postgraduálneho štúdia vo fyzike študoval telekomunikácie na Fakulte elektrotechniky a informatiky Slovenskej technickej univerzity v Bratislave. Po ukončení štúdia sa venoval viac ako 10 rokov vedeckej a pedagogickej činnosti na FMFI UK Bratislava. Plánoval sa venovať fyzike zvyšok života; avšak potešenie z programovania a budovania IT riešení, kde mohol efektívne dosiahnuť skutočný pokrok a vidieť výsledky, postupne rástlo. V posledných rokoch vybudoval riešenia a technologické stacky pre projekty z rôznych oblastí ako telco, bankovníctvo, poistenie, cestovanie, herný priemysel, data mining a 3D modelovanie. Jeho motto je: "Dokonalosť nie je dosiahnutá vtedy, keď už niet čo pridať, ale keď nezostane nič, čo by sa dalo odobrať."`,
        img: "https://media.licdn.com/dms/image/C4E03AQHypVTaBuWf-g/profile-displayphoto-shrink_200_200/0?e=1556755200&v=beta&t=UJLlI_RBKalLZxqtkEdk4cpoVG3eJNDWZejjvAsKFww",
        linkedin: "https://www.linkedin.com/in/peter-rybar/"
    },
    {
        name: "René Darmoš",
        position: "Founder, CFO",
        bio: ``,
        img: "https://media.licdn.com/dms/image/C5603AQFdcVRvSaxLXQ/profile-displayphoto-shrink_200_200/0?e=1579737600&v=beta&t=rMzMZ0lWK6Tg3-IM2d0bPoUfOCz6a7qiKOj4QgAhLpk",
        linkedin: "https://www.linkedin.com/in/rendolf/"
    },
    {
        name: "Daniel Derevjanik",
        position: "Founder, CTO",
        bio: `V priebehu 5 ročnej praxe stihol Daniel pracovať na rôznorodých projektov, z jednej z prvých integrácií strojového učenia s cieľom na zlepšenie Google Adwords služieb, cez prototypovania aplikácii pre poprednu medzinárodnú spoločnosť, vedenie technologickej inovácie webovej aplikácie, práca na globalnej streaming platforme a momentálne rozvíja infraštruktúru pre firmu zaoberajúcu sa kryptografiou hesiel. Popri práci na komerčných projektoch absolvoval Daniel bakalársky stupeň na Fakulte matematiky, fyziky a informatiky Univerzity Komenského v Bratislave. Okrem toho, sa Daniel zaujíma aj Open Source komunitu, do ktorej čas od času prispeje. Vďaka širokému spektru pracovných skúsenosti a dobrou znalosťou Open Source komunity, je Daniel vitálnou súčasťou tímu.`,
        img: "https://media.licdn.com/dms/image/C4E03AQHcPce3zk4UNQ/profile-displayphoto-shrink_800_800/0?e=1556755200&v=beta&t=cl__KDqkqzEHnc36eSqGJxkM36A835Svema33nwjcG4",
        linkedin: "https://www.linkedin.com/in/danielderevjanik/"
    },
    {
        name: "Evan Šanta",
        position: "Founder, Object recognition",
        bio: ``,
        img: "https://media.licdn.com/dms/image/C5603AQEWThRcccf5uw/profile-displayphoto-shrink_800_800/0?e=1556755200&v=beta&t=2YkvgwbxenPR57BZHa1lkSfm_1sF92Muj-yy9djbgew",
        linkedin: "https://www.linkedin.com/in/evan-%C5%A1anta/"
    }
];

export function team(): HsmlFragment {
    return [
        ["div.w3-content", [
            ["h1", ["Team"]],
            ["ul.w3-ul.w3-card-4-.w3-white-",
                founders.map<HsmlElement>(t => (
                    ["li.w3-bar", {}, [
                        ["a", { href: t.linkedin, title: "LinkedIn", target: "_blank", rel: "noopener" }, [
                            // ["img.w3-bar-item.w3-circle", { src: t.img, style: "width:85px" }],
                            ["div.w3-bar-item", {}, [
                                ["strong.w3-large", [t.name]],
                                ["br"],
                                ["span", [t.position]],
                                // ["br"],
                                // ["span", [t.bio]],
                                // ["br"],
                                // ["a.w3-button-",
                                //     { href: t.linkedin, title: "LinkedIn", target: "_blank", rel: "noopener" },
                                //     [["i.fa.fa-fw.fa-linkedin"]]
                                // ]
                            ]]
                        ]]
                    ]]
                ))
            ]
        ]]
    ];
}
