import { HsmlFragment, HsmlElement } from "peryl/dist/hsml";

export interface Menu {
    url: string;
    label: string;
    icon: string;
}

export function sidebar(menu: Menu[], activeMenuItem: string): HsmlFragment {
    const nbsp = "\u00a0 ";
    return [
        ["nav", {}, [
            ["br"],
            ["div.w3-container.w3-row", {}, [
                ["div.w3-col.s4", [
                    ["img.w3-circle-.w3-margin-right",
                        {
                            src: "/assets/icons/android-chrome-192x192.png",
                            style: "width:50px"
                        }
                    ]
                ]],
                ["div.w3-col.s8.w3-bar", [
                    ["span", [
                        ["a", { href: "index.html", style: "text-decoration: none;" }, [
                            ["strong.w3-large", ["Open Parking Data"]],
                            ["br"],
                            "Community driven"
                            // ["br"],
                            // "Open Source platform"
                        ]]
                    ]],
                    ["br"],
                    // ["a.w3-bar-item.w3-button",
                    //     { href: "#messages", title: "Messages" },
                    //     [["i.fa.fa-envelope"]]
                    // ],
                    // ["a.w3-bar-item.w3-button",
                    //     { href: "#profile", title: "Profile" },
                    //     [["i.fa.fa-user"]]
                    // ],
                    // ["a.w3-bar-item.w3-button",
                    //     { href: "#settings", title: "Settings" },
                    //     [["i.fa.fa-cog"]]
                    // ]
                ]]
            ]],
            ["hr"],
            // ["div.w3-container", [
            //     ["h5", ["Menu"]],
            // ]],
            ["div.w3-bar-block", {}, [
                ...menu.map<HsmlElement>(m => (
                    ["a.w3-bar-item.w3-button.w3-padding",
                        {
                            href: m.url,
                            classes: [["w3-light-grey", m.url === activeMenuItem]]
                        },
                        [
                            [m.icon], nbsp, m.label
                        ]
                    ])
                ),
                ["hr"],
                ["div.w3-center", {}, [
                    ["a.w3-button",
                        {
                            href: "http://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fopen-parking-data.gitlab.io&title=Peter Rybar&source=https%3A%2F%2Fopen-parking-data.gitlab.io",
                            title: "Share on LinkedIn",
                            target: "_blank",
                            rel: "noopener"
                        },
                        [["i.fab.fa-fw.fa-linkedin-in"]]
                    ],
                    ["a.w3-button",
                        {
                            href: "https://twitter.com/intent/tweet?source=https%3A%2F%2Fopen-parking-data.gitlab.io&text=Peter Rybar:%20https%3A%2F%2Fopen-parking-data.gitlab.io",
                            title: "Tweet",
                            target: "_blank",
                            rel: "noopener"
                        },
                        [["i.fab.fa-fw.fa-twitter"]]
                    ],
                    ["a.w3-button",
                        {
                            href: "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fopen-parking-data.gitlab.io&quote=Peter Rybar",
                            title: "Share on Facebook",
                            target: "_blank",
                            rel: "noopener"
                        },
                        [["i.fab.fa-fw.fa-facebook-f"]]
                    ],
                    ["br"],
                    "Share"
                ]],
                ["br"],
                ["br"]
            ]]
        ]]
    ];
}
