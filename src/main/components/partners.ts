import { HsmlFragment, HsmlElement } from "peryl/dist/hsml";

const members = [
    {
        name: "DEPARTMENT OF APPLIED INFORMATICS",
        link: "https://dai.fmph.uniba.sk/",
        img: ""
    },
    {
        name: "GOSPACE",
        link: "https://www.gospace.sk/",
        img: ""
    },
    {
        name: "MAKERS",
        link: "https://www.makers.sk/",
        img: ""
    },
    {
        name: "zymestic",
        link: "http://www.zymestic.sk/",
        img: ""
    },
    {
        name: "BIOTRON",
        link: "https://biotron.io/",
        img: ""
    }
];


export function partners(): HsmlFragment {
    return [
        ["div.w3-content", [
            ["h1", ["Partners"]],
            ["ul.w3-ul.w3-card-4-.w3-white-",
                members.map<HsmlElement>(t => (
                    ["li.w3-bar", {}, [
                        ["a", { href: t.link, target: "_blank", rel: "noopener" }, [
                            t.img
                                ? ["img.w3-bar-item.w3-circle", { src: t.img, style: "width:85px" }]
                                : "",
                            ["div.w3-bar-item", {}, [
                                ["strong.w3-large", [t.name]],
                                ["br"],
                                ["span", [t.link]]
                            ]]
                        ]]
                    ]]
                ))
            ]
        ]]
    ];
}
