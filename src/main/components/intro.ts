import { HsmlFragment } from "peryl/dist/hsml";

export function intro(): HsmlFragment {
        // http://inrix.com/blog/2018/10/inrix-parking-data-coverage-expansion/
    return [
        ["div.w3-content", [
            ["h1", ["Open Parking Data"]],
            ["p", [
                "World-wide, community-based, open-source parking data platform.",
                ["strong", ["Smart parking solution"]],
                ` makes parking spaces easier to find but the real value is in the`,
                ["strong", ["community, data and analysis"]], "."
            ]],
            ["h2", ["Pitch"]],
            ["p", [
                ["iframe", {
                    src: "https://docs.google.com/presentation/d/e/2PACX-1vTURz2PiPE3KfWzweRzF3o0BVNQ8omkFZ4REBL5gKadWhHryTJZ90GPy9qICuFTU3bLcqjDZ93cUD1W/embed?start=true&loop=true&delayms=5000",
                    frameborder: "0",
                    width: "100%",
                    height: "500",
                    allowfullscreen: "true" ,
                    mozallowfullscreen: "true" ,
                    webkitallowfullscreen: "true"
                }, [""]]
            ]],
            ["h2", ["Goals"]],
            ["ul", [
                ["li", ["Develop global, community driven parking data collection platform"]],
                ["li", ["Build open source, community software tools for data acquisition"]],
                ["li", ["Define worldwide protocol and expose data via API for third parties to build applications"]],
                ["li", ["Expose smart data analytic for cities, cars and navigation systems"]],
                ["li", ["Unify community, public and commercial parking data set"]],
                ["li", ["Allow community or commercial parking space sharing during unoccupied hours - Airbnb of parking"]],
                ["li", ["Integrate payment systems for instant payments - blockchain based credit system"]]
            ]],
            ["h2", ["Cases"]],
            ["h4", ["Data acquisition - community, IoT"]],
            ["p", [`
                Collect data from many different sources. The data differs in type (e.g. on- and off-street), formats and transfer methods. The data comes from a multitude of vendors.
                Once data is securely stored, it is transformed into our vendor-independent data model. All along the way, data quality controls are in place.
                Community data collection is supported by open source and IoT community tools.
            `]],
            ["h4", ["Community credit-based shared parking spaces"]],
            ["p", [`
                There's so much parking space that we could be using that is empty.
                Every time you go to work your car space is empty and someone else could be using it.
                Sometimes You don't actually have to build parking infrastructure, because there is already a fair bit of it around.
            `]],
            ["h4", ["Rent private space during unoccupied hours"]],
            ["p", [`
                Do you have parking spaces or boat moorings which could be making you money?
                OPD can manage them for you! Just list your empty parking space on OPD to make fair income every month. Airbnb of parking!
            `]],
            ["h4", ["Public accessible data for third-parties"]],
            ["p", [`
                Platform supports complementary third-parties systems and offer interface
                between the platform and external applications like navigation systems,
                driverless cars, smart city’s parking guidance system or time table information from public transport.
            `]],
            ["h4", ["Smart city planning based on parking data analysis"]],
            ["p", [`
                Open parking data is a key. Understanding driver patterns and behavior can help
                city planners optimize the flow of traffic and reduce congestion – adapting parking
                fees accordingly can encourage or discourage motorists into or away from
                certain areas (e.g. city center) at peak times (e.g. at Christmas or during rush hour).
                Valuable information is accessible through online reporting tool. More in-depth analyses are available on demand.
            `]],
            ["h4", ["Manage foundation to support related subprojects and technology research"]],
            ["p", [`
                Create a foundation as a legal category of nonprofit organization that will either
                donate funds to and support other related subprojects and organizations,
                or provide the source of funding for its own research purposes.
                Foundation will incorporate public foundations to pool funds (e.g. a community foundation)
                and private foundations who are typically endowed by an individual or organizations.
            `]],
            ["h4", ["Car Theft Prevention"]],
            ["p", [`
                Open parking data can be used to setup watchdog for car occurrence on parking lot.
                Car owner can be notify in case of theft and call police instantly.
                Car anti theft watchdog system can be integrated to police department information
                systems to get notified police officer directly in particular crime area.
            `]],
            ["h2", ["Join us"]],
            ["p", [`
                We are looking for an investor or partner for the project
                that develops software to monitor the occupancy of parking places
                in towns by a community of volunteers.
                Data will be sold commercially, and part of the profit will go to
                the community to collect the data using community software.
            `]],
            ["h2", ["Presentation"]],
            ["p", [
                ["iframe", {
                    src: "https://docs.google.com/presentation/d/e/2PACX-1vQcP_T21fet3kHKzcmFBTw_f9COWaIMRkE8tsoMvV5Oukh-jWyx0g0-Xc54pVV7tfn-vJRm9dZbas8N/embed?start=false&loop=true&delayms=5000",
                    frameborder: "0",
                    width: "100%",
                    height: "500",
                    allowfullscreen: "true" ,
                    mozallowfullscreen: "true" ,
                    webkitallowfullscreen: "true"
                }, [""]]
            ]]
        ]]
    ];
}
