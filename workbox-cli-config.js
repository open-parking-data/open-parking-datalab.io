module.exports = {
  "globDirectory": "dist/",
  "globPatterns": [
    "**/*.{css,woff2,png,js,html,json}"
  ],
  "swSrc": "dist/service-worker.js",
  "swDest": "dist/service-worker.js",
  "globIgnores": [
    "../workbox-cli-config.js"
  ]
};
